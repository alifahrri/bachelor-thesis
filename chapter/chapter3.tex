\makeatletter
\def\input@path{{../}}
\makeatother
\documentclass[../thesis.tex]{subfiles}
\begin{document}

In this chapter, we discussed our approach to solve the problem of motion planning for omnidirectional robot in dynamic robot soccer environment. First, discuss our approach to simplify the geometry and dynamics of the robots. Then, we derive the fixed-state free-final-time controller for double integrator model. After that, we extend the collision-checking algorithm in the previous chapter to deal with trajectory and moving obstacle. Then, we discuss stochastic decision to directly sampling the goal state. After that, we discuss our implementation of kinodynamic-RRT* algorithm with double integrator model in dynamic environment. Then we describe our motion planning strategy. After that, we discuss trajectory tracking control used in this work to follow the planned trajectory. Finally, we describe our approach for online computation of motion planning and trajectory tracking.

\section{Geometry Simplification of Robot and Obstacles}

In this section, we describe our strategy for simplyfing the geomoetry of the robot and obstacles. In order to simplify the collision checking algorithm needed by kinodynamic-RRT*, we simplify the robot as a point and the obstacles as circular objects.

\begin{figure}[ht!]
	\centering
	\includegraphics[width=0.5\textwidth]{geom}
	\caption{Geometry Simplification of the robot and Obstacles}
	\label{geom_simple}
\end{figure}

Figure \ref{geom_simple} shows the illustration of this simplification. In the figure, the robot is shown as a yellow point with dashed line in red and yellow represent its orientation in the world frame which is shown as blue point. The obstacles are depicted in gray circles with its center point position and radius are ilustrated with $p_0$ and $\rho$, respectively. By simplifying the robot's geometry as a point, the rotational motion of the robot does not contribute to the collision checking.

\section{Dynamic Models of the Robot}

Having a simplified geometric model of the robot, we discussed the dynamic model of the robot in this section. In this work, we considered the \emph{NuBot} robot model from Yu et al. in \cite{yu2010nubot}. To simplify the dynamic of the robot for , treated the robot as \emph{point-mass} object obeying Newton's Law of Motion. Since the robot are able to move in any translational and rotational simultaneusly, we also decoupled the robot's motion in translational and rotational direction. We also assume a \emph{frictionless} ground plane for the purpose of motion planning. The motion in $x$ and $y$ direction as well as rotational direction, $\omega$, are assumed to be independent from each other, so that :

\begin{equation}
\ddot{x} = \frac{1}{m}f_x,\; \ddot{y} = \frac{1}{m}f_y,\; \ddot{\omega} = \frac{1}{m}f_{\omega}
\end{equation}
where $f_x,f_y,$ and $f_{\omega}$ are the force acting in each direction, and $m$ is the mass of the robot which is a constant $31$ kg \cite{yu2010nubot}.

The dynamic of the robot in translational direction is modeled as a double integrator in two-dimensional plane so that its state-space representation is expressed by :

\begin{equation} \label{dbl_int_state_space_eq}
\dot{\boldsymbol{x}} = \begin{bmatrix}
0 & I \\
0 & 0
\end{bmatrix}\boldsymbol{x} + \begin{bmatrix}
0 \\
I
\end{bmatrix}\boldsymbol{u}
\end{equation}
where, 

\begin{equation*} \label{dbl_int_label_eq}
\boldsymbol{x} = 
\begin{bmatrix}
\boldsymbol{p} \\
\boldsymbol{v}
\end{bmatrix},
\;
\boldsymbol{u} = \boldsymbol{a},
\end{equation*}
\begin{equation*}
\boldsymbol{p} = 
\begin{bmatrix} 
p_x \\
p_y
\end{bmatrix},
\;
\boldsymbol{v} =
\begin{bmatrix}
v_x \\
v_y
\end{bmatrix}
\end{equation*}
\begin{equation*}
\boldsymbol{a} = 
\begin{bmatrix}
a_x \\
a_y
\end{bmatrix} = 
\frac{1}{m}
\begin{bmatrix}
f_x \\
f_y
\end{bmatrix}
\end{equation*}
where $p_x, p_y, v_x, v_y, a_x,$ and $a_y$ are the position, velocity, and acceleration of the robot in the $x$, and $y$ direction in the world frame, respectively.

%The rotational motion could be expressed as :
%
%\begin{equation}
%\omega = \int \dot{\omega}(t) dt
%\end{equation}

\section{Implementation of Kinodynamic-RRT* for Double Integrator Model}
In this section, we describe our implementation of the kinodynamic-RRT* for double integrator model including other necessary modules needed by the algorithm. This section starts by deriving the open-loop optimal controller to determine the trajectory of the robot connecting two states. Then we describe our implementation trajectory collision checking with moving obstacles. We also discuss our goal sampling strategy to further improves the quality of the solution trajectory. Finally, we discuss the implementation of the kinodynamic-RRT* for double integrator model in the presence of moving obstacles.

\subsection{Fixed-State Free-Final-Time Controller for Double Integrator Model}
  
This subsection describe the derivation of fixed-state free-final-time controller from general solution described in the previous chapter. 
Let us rewrite the state-space equation for double integrator model :

\begin{equation} \label{dbl_int_state_space_eq}
\dot{\boldsymbol{x}} = \begin{bmatrix}
0 & I \\
0 & 0
\end{bmatrix}\boldsymbol{x} + \begin{bmatrix}
0 \\
I
\end{bmatrix}\boldsymbol{u}
\end{equation}
We also defined the input weighting matrix for the double integrator model as :
\begin{equation} \label{dbl_int_input_mat_eq}
R = rI
\end{equation}
where $r$ is the input weighting constant.

To solve the optimal trajectory as defined in the (\ref{composite_state_space_eq}), we first solve the weighted controllability Gramian and the optimal cost of the double integrator model. This could be done by filling the (\ref{dbl_int_state_space_eq}) to (\ref{gramian_eq}):

\begin{equation} \label{dbl_int_gramian_eq}
G(t)=\begin{bmatrix}
\frac{t^{3}}{3r} & 0 & \frac{t^{2}}{2r} & 0 \\
0 & \frac{t^{3}}{3r} & 0 & \frac{t^{2}}{2r} \\
\frac{t^{2}}{2r} & 0 & \frac{t}{r} & 0\\
0 & \frac{t^{2}}{2r} & 0 & \frac{t}{r}
\end{bmatrix}
\end{equation}
and the inverse of the weighted controllability Gramian matrix is given by :

\begin{equation} \label{dbl_int_inv_gramian_eq}
G^{-1}(t) =\begin{bmatrix}
\frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} & 0 \\
0 & \frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} \\
\frac{-6r}{t^{2}} & 0 & \frac{4r}{t} & 0 \\
0 & \frac{-6r}{t^{2}} & 0 & \frac{4r}{t}
\end{bmatrix}
\end{equation}
and 
\begin{equation} \label{ext_dbl_int_eq}
\bar{\boldsymbol{x}}(t) =
\begin{bmatrix}
1 & 0 & t & 0 \\
0 & 1 & 0 & t \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{x}_i
\end{equation}
so that,
\begin{equation} \label{d_sym_dbl_int_eq}
\boldsymbol{d}(\tau) =
\begin{bmatrix}
\frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} & 0 \\
0 & \frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} \\
\frac{-6r}{t^{2}} & 0 & \frac{4r}{t} & 0 \\
0 & \frac{-6r}{t^{2}} & 0 & \frac{4r}{t}
\end{bmatrix}\left \{
\boldsymbol{x}_f -
\begin{bmatrix}
1 & 0 & t & 0 \\
0 & 1 & 0 & t \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{x}_i \right \},
\end{equation}
define $\boldsymbol{x}_f = \begin{bmatrix}
p_{xf} & p_{yf} & v_{xf} & v_{yf}
\end{bmatrix}^{T}$ and $\boldsymbol{x}_i = \begin{bmatrix}
p_{xi} & p_{yi} & v_{xi} & v_{yi}
\end{bmatrix}^{T}$, we could expand the equation \ref{d_sym_dbl_int_eq} as :
\begin{equation*}
\boldsymbol{d}(\tau) =
\begin{bmatrix}
\frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} & 0 \\
0 & \frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} \\
\frac{-6r}{t^{2}} & 0 & \frac{4r}{t} & 0 \\
0 & \frac{-6r}{t^{2}} & 0 & \frac{4r}{t}
\end{bmatrix}
\begin{bmatrix}
p_{xf} - p_{xi} - v_{xi}t\\
p_{yf} - p_{yi} - v_{yi}t\\
v_{xf} - v_{xi}\\
v_{yf} - v_{yi}
\end{bmatrix},
\end{equation*}
\begin{equation*}
\boldsymbol{d}(\tau) =
\begin{bmatrix}
\frac{-6r(v_{xf}-v_{xi})}{t^2} + \frac{12r(p_{xf}-p_{xi}-v_{xi}t)}{t^3}\\
\frac{-6r(v_{yf}-v_{yi})}{t^2} + \frac{12r(p_{yf}-p_{yi}-v_{yi}t)}{t^3} \\
\frac{4r(v_{xf}-v_{xi})}{t} - \frac{6r(p_{xf}-p_{xi}-v_{xi}t)}{t^2}\\
\frac{4r(v_{yf}-v_{yi})}{t} - \frac{6r(p_{yf}-p_{yi}-v_{yi}t)}{t^2}
\end{bmatrix}
\end{equation*}
and we have closed expression of the derivative of the cost :
\begin{equation} \label{dbl_int_cost_derivative_eq}
\dot{c}(\tau) = 1-2\left \{ \begin{bmatrix}
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0
\end{bmatrix}
\boldsymbol{x}_f
\right \}^{T}\boldsymbol{d}(\tau)-
\boldsymbol{d}(\tau)^{T}\begin{bmatrix}
0 & 0 \\
0 & 0 \\
1 & 0 \\
0 & 1
\end{bmatrix}
\begin{bmatrix}
\frac{1}{r} & 0\\
0 & \frac{1}{r}
\end{bmatrix}
\begin{bmatrix}
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{d}(\tau),
\end{equation}
\begin{comment}
\begin{equation*}
\dot{c}(\tau) = 1-2\left \{ \begin{bmatrix}
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0
\end{bmatrix}
\boldsymbol{x}_f
\right \}^{T}\boldsymbol{d}(\tau)-
\boldsymbol{d}(\tau)^{T}
\begin{bmatrix}
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & \frac{1}{r} & 0 \\
0 & 0 & 0 & \frac{1}{r}
\end{bmatrix}
\boldsymbol{d}(\tau)
\end{equation*}
\end{comment}
and the cost :
\begin{equation} \label{dbl_int_cost_eq}
c(\tau) =
\tau +
\left \{  
\boldsymbol{x}_f -
\begin{bmatrix}
1 & 0 & t & 0 \\
0 & 1 & 0 & t \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{x}_i
\right \}^{T}
\begin{bmatrix}
\frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} & 0 \\
0 & \frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} \\
\frac{-6r}{t^{2}} & 0 & \frac{4r}{t} & 0 \\
0 & \frac{-6r}{t^{2}} & 0 & \frac{4r}{t}
\end{bmatrix}
\left \{  
\boldsymbol{x}_f -
\begin{bmatrix}
1 & 0 & t & 0 \\
0 & 1 & 0 & t \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{x}_i
\right \}
\end{equation}
and the composite derivative equation is given by :
\begin{equation} \label{dbl_int_composite_derivative_eq}
\\\begin{bmatrix}
\dot{\boldsymbol{x}}(t)\\
\dot{\boldsymbol{y}}(t)
\end{bmatrix}
=
\begin{bmatrix}
0 & I & 0 & 0 \\
0 & 0 & 0 & \begin{bmatrix}
\frac{1}{r} & 0\\
0 & \frac{1}{r}
\end{bmatrix}\\
0 & 0 & 0 & 0 \\
0 & 0 & -I & 0
\end{bmatrix}
\begin{bmatrix}
\boldsymbol{x}(t)\\
\boldsymbol{y}(t)
\end{bmatrix},
\begin{bmatrix}
\boldsymbol{x}(\tau^{*})\\
\boldsymbol{y}(\tau^{*})
\end{bmatrix}
=
\begin{bmatrix}
\boldsymbol{x}_f\\
\boldsymbol{d}(\tau^{*})
\end{bmatrix}
\end{equation}
and the solution to (\ref{dbl_int_composite_derivative_eq}) is :

\begin{equation} \label{dbl_int_composite_state_eq}
\\\begin{bmatrix}
\boldsymbol{x}(t)\\
\boldsymbol{y}(t)
\end{bmatrix}
=
\begin{bmatrix}
I &
\begin{bmatrix}
t-\tau & 0 \\
0 & t-\tau
\end{bmatrix} &
\begin{bmatrix}
\frac{(-t+\tau)(t-\tau)^2}{6r} & 0 \\
0 & \frac{(-t+\tau)(t-\tau)^2}{6r}
\end{bmatrix} & \begin{bmatrix}
\frac{(t-\tau)^2}{2r} & 0 \\
0 & \frac{(t-\tau)^2}{2r}
\end{bmatrix}
\\
0 & I & \begin{bmatrix}
\frac{(-t+\tau)(t-\tau)}{2r} & 0 \\
0 & \frac{(-t+\tau)(t-\tau)}{2r}
\end{bmatrix} &
\begin{bmatrix}
\frac{t-\tau}{r} & 0\\
0 & \frac{t-\tau}{r}
\end{bmatrix}\\
0 & 0 & I & 0 \\
0 & 0 & \begin{bmatrix}
-t+\tau & 0 \\
0 & -t+\tau
\end{bmatrix} & I
\end{bmatrix}
\begin{bmatrix}
\boldsymbol{x}_f\\
\boldsymbol{d}(\tau^{*})
\end{bmatrix}
\end{equation}

Equation (\ref{dbl_int_composite_state_eq}) above solves the trajectory connecting initial state $x_i$ to final stat $x_f$ satisfying double integrator dynamics. This trajectory solver is used by the following Kinodynamic RRT* algorithm to determine the connection between state.

Having closed-form solution of the optimal trajectory in (\ref{dbl_int_composite_state_eq}), we still have to solve the optimal arrival time, $\tau^{*}$. To find the optimal arrival time, as suggested in \ref{webb2013kinodynamic}, we take the positive value of root of the derivative of the optimal cost $\dot{c}(\tau)$. In this work, we use secant-method to find the root of $\dot{c}(\tau)$. Since the optimal time should be a positive value, we simply restart the search if it found negative value. The complete procedure to solve the optimal trajectory is shown in algorithm \ref{alg_trajectory_solver}.

\begin{algorithm}
\begin{algorithmic}[1]
\Function{TrajectorySolver}{$x_i, x_f$}
	\State $\func{set initial guess}\; \tau_{guess,-1}, \tau_{guess,0} $
	\State $e_s, i_{max}, e_a$
	\State $\tau^{*} = \textsc{SecantMethod}(\tau_{guess,-1},\tau_{guess,0},e_s,i_{max},e_a)$
	\While {$\tau < 0$}
		\State $\func{re-set initial guess}\; \tau_{guess,-1}, \tau_{guess,0} $
		\State $\tau^{*} = \textsc{SecantMethod}(\tau_{guess,-1},\tau_{guess,0},e_s,i_{max},e_a)$
	\EndWhile
	\State $\func{compute}\; \boldsymbol{d}(\tau^{*})$
	\For{$t:dt:\tau^{*}$}
		\State $\pi^{*}(t) \leftarrow \func{compute equation}\; \ref{dbl_int_composite_state_eq}\; \func{at time}\; t$
	\EndFor
    \Return {$\pi^{*}$}
\EndFunction
\end{algorithmic}
\caption{Trajectory Solver for Double Integrator Model}
\label{alg_trajectory_solver}
\end{algorithm}

\subsection{Trajectory Collision-Checking with Moving Obstacles}

We implement collision checking algorithm in dynamic environment by extending the algorithm \ref{collision_alg} to deal with trajectory and moving obstacles as shown in algorithm \ref{trajectory_collision_alg}. The extended collision checking algorithm is done by approximating the trajectory as sequence of lines and checking the collision with each moving obstacles for each time slice, hence reduce the problem as a collection of line circles collision checking problem that could be solved using algorithm \ref{collision_alg}.

\begin{algorithm}
\begin{algorithmic}[1]
\Function{TrajectoryCollision}{$\vars{trajectory}, \vars{DynamicObstacles}$}
	\For{$(x_0,y_0,x_1,y_1,t_0,t_1) \in \vars{trajectory}$}
		\State $\vars{Obstacles} \leftarrow \vars{DynamicObstacles} \;\func{at time}\; t_0 \;\func{and} \;t_1$
		\State $\vars{collision} = \textsc{LineCirclesCheck}(x_0,y_0,x_1,y_1,\vars{Obstacles})$
		\If{$\vars{collision}$}
			\Return {$\vars{True}$}
		\EndIf
	\EndFor
    \Return {$\vars{False}$}
\EndFunction
\end{algorithmic}
\caption{Trajectory Collision Checking with Moving Obstacles}
\label{trajectory_collision_alg}
\end{algorithm}

The algorithm \ref{trajectory_collision_alg} takes the trajectory and the dynamic obstacles' state as input and return whether the specified trajectory collides with the dynamic obstacles. Line $3$ in the algorithm calculate the positions of each obstacles given the obstacles' state. We assume that the obstacles are moving with constant velocity, hence for each obstacle, the position of the obstacles at time $t$ could be calculated using equation \ref{dynamic_obs_x} and \ref{dynamic_obs_y} :

\begin{equation*}
 D^{'}_{x}(t) = D^{'}_{x}(0) + v_{D_x}t
\end{equation*}
\begin{equation*}
 D^{'}_{y}(t) = D^{'}_{y}(0) + v_{D_y}t
\end{equation*}

While this approximation of obstacles motion are not correct in long term, this enabled the planner to anticipate the obstacles' motions in shorter term and gives the planner to generate safer trajectory plan.

\subsection{Stochastic Decision to Directly Sampling Goal State}

To improve the quality and consistency of the planned trajectory, we implements stochastic decision for the sampling procedure of the kinodynamic-RRT* as proposed in \cite{bruce2002real}. The stochastic decision decide whether or not to directly sampling the goal state:

\begin{itemize}
\item with probability $p$, directly sampling the goal state,
\item with probability $1-p$, randomly sample the state from uniform distribution.
\end{itemize}

\begin{algorithm}[H]
\begin{algorithmic}[1]
\Function{Sample}{$\boldsymbol{q}_{goal},p$}
	% \State $X \sim \operatorname{Bern} \left({p}\right)$
	\State $\rho\leftarrow \operatorname{Bern} \left({p}\right)$
	\If {$\rho$}
		\Return $\boldsymbol{q}_{goal}$
	\Else
		\Return $\func{random sample from}\; \textit{U}(\boldsymbol{q}_{min},\boldsymbol{q}_{max})$
	\EndIf
\EndFunction
\end{algorithmic}
\caption{Sampling with Stochastic Decision}
\label{sample_direct}
\end{algorithm}
Algorithm \ref{sample_direct} shows the implemented sample procedure with stochastic decision to directly sample the goal state. Line $2$ from the algorithm draw a boolean value from Bernoulli distribution with probability $p$, $\operatorname{Bern} \left({p}\right)$, to variable $\rho$. If $\rho$ is true, then the algorithm returns the goal state, else, the algorithm randomly sample the state from uniform distribution, $\textit{U}$, with lower bound $\boldsymbol{q}_{min}=[x_{min},y_{min},\dot{x}_{min},\dot{y}_{min}]^T$ and upper bound $\boldsymbol{q}_{max}=[x_{max},y_{max},\dot{x}_{max},\dot{y}_{max}]^T$. The lower and upper bound of the sampled state could be determine by the size of the field and the target velocity of the robot. 

\subsection{Kinodynamic-RRT* for Double Integrator Model in Dynamic Environment}

In this subsection we discuss our implementation of the kinodynamic-RRT* with double integrator model to deal with dynamic environment that includes moving obstacles. Furthermore, we also apply stochastic decision to directly sampling the goal state as described in the previous section to the algorithm. Algorithm \ref{impl_rrt} shows the implemented kinodynamic-RRT* algorithm. The algorithm takes initial state, $\boldsymbol{q}_{start}=[x_{start},y_{start},\dot{x}_{start},\dot{y}_{start}]^T$, goal state, $\boldsymbol{q}=[x_{goal},y_{goal},0.0,0.0]^T$, probability of direct sampling, $p$, and maximum number of iteration $n_{iter}$. Unlike the original kinodynamic-RRT*, we restrict the number of iteration to $n_{iter}$, instead of infinitely growing the tree. The $\func{Sample}$ procedure in line $4$ calls the algorithm \ref{sample_direct}.  The neighboring states, $\boldsymbol{q}_{neighbor}$, of the sampled state is chosen so that they are the nodes that already on the tree and the trajectory connecting the neighbor state to the sampled state is not in collision with the obstacle. 

\begin{algorithm}[H]
\begin{algorithmic}[1]
\Function{DoubleIntKinodynamic-RRT*}{$\boldsymbol{q}_{start}, \boldsymbol{q}_{goal}, p, n_{iter}$}
	\State $\mathcal{T} \leftarrow \{\boldsymbol{q}_{start}\}$
	\For{ $i\in [1,n_{iter}]$ }
		\State $\boldsymbol{q}_i \leftarrow \func{Sample}(\boldsymbol{q}_{goal})$
		\State $\boldsymbol{q}_{neighbor} \leftarrow \mathcal{T} \mid c^{*}(\boldsymbol{q},\boldsymbol{q}_i) < r \wedge \, not \,\textsc{TrajectoryCollision}(\pi^{*}(\boldsymbol{q},\boldsymbol{q}_i))$
		\State $\boldsymbol{q} \leftarrow \func{argmin} \left(\{ \boldsymbol{q} \in \boldsymbol{q}_{neighbor} \right\}(c^{*}(\boldsymbol{q},\boldsymbol{q}_i))$
		\State $parent(\boldsymbol{q}_i) \leftarrow \boldsymbol{q}$
		\State $cost(\boldsymbol{q}_i) \leftarrow cost(\boldsymbol{q}) + c^{*}(\boldsymbol{q},\boldsymbol{q}_i)$
		\State
			$\boldsymbol{q}_{neighbor} \leftarrow \mathcal{T} \cup \{q_{goal}\} \mid c^{*}(\boldsymbol{q}_i,\boldsymbol{q}) < r $
			\par\hskip\algorithmicindent $\wedge \, not \, \textsc{TrajectoryCollision}(\pi^{*}(\boldsymbol{q}_i,\boldsymbol{q}))$
			%\end{varwidth}
		\For{$\boldsymbol{q} \in \boldsymbol{q}_{neighbor}$}
			\If{$cost(\boldsymbol{q}_i)+c^{*}(\boldsymbol{q}_i,\boldsymbol{q})<cost(\boldsymbol{q})$}
				\State $parent(\boldsymbol{q}) \leftarrow \boldsymbol{q}_i$
				\State $cost(\boldsymbol{q}) \leftarrow cost(\boldsymbol{q}_i)+c^{*}(\boldsymbol{q}_i,\boldsymbol{q})$
			\EndIf
		\EndFor
		\If{$parent(q_i)$}
			\State $\mathcal{T}\leftarrow\mathcal{T}\cup q_i$
		\EndIf
	\EndFor
	\State $\boldsymbol{q}_{solution}[]\leftarrow\emptyset$
	\State $\boldsymbol{q}\leftarrow\boldsymbol{q}_{goal}$
	\While {$parent(\boldsymbol{q})$}
		\State $\func{insert}\;\pi^{*}(parent(\boldsymbol{q}),\boldsymbol{q})\; \func{to}\; \boldsymbol{q}_{solution}[]$
		\State $\boldsymbol{q}\leftarrow parent(\boldsymbol{q})$
	\EndWhile
	\Return $\boldsymbol{q}_{solution}[]$
\EndFunction
\end{algorithmic}
\caption{Implemented Kinodynamic-RRT* Algorithm}
\label{impl_rrt}
\end{algorithm}

The optimal control policy, $\pi^{*}$, in line $5$ and $9$ implements the fixed-final-state free-final-time control for double integrator model from equation \ref{dbl_int_composite_state_eq}:

\begin{equation*}
\\\begin{bmatrix}
\boldsymbol{x}(t)\\
\boldsymbol{y}(t)
\end{bmatrix}
=
\begin{bmatrix}
I &
\begin{bmatrix}
t-\tau^{*} & 0 \\
0 & t-\tau^{*}
\end{bmatrix} &
\begin{bmatrix}
\frac{(-t+\tau)(t-\tau^{*})^2}{6r} & 0 \\
0 & \frac{(-t+\tau^{*})(t-\tau^{*})^2}{6r}
\end{bmatrix} & \begin{bmatrix}
\frac{(t-\tau^{*})^2}{2r} & 0 \\
0 & \frac{(t-\tau^{*})^2}{2r}
\end{bmatrix}
\\
0 & I & \begin{bmatrix}
\frac{(-t+\tau^{*})(t-\tau^{*})}{2r} & 0 \\
0 & \frac{(-t+\tau^{*})(t-\tau^{*})}{2r}
\end{bmatrix} &
\begin{bmatrix}
\frac{t-\tau^{*}}{r} & 0\\
0 & \frac{t-\tau^{*}}{r}
\end{bmatrix}\\
0 & 0 & I & 0 \\
0 & 0 & \begin{bmatrix}
-t+\tau^{*} & 0 \\
0 & -t+\tau^{*}
\end{bmatrix} & I
\end{bmatrix}
\begin{bmatrix}
\boldsymbol{x}_f\\
\boldsymbol{d}(\tau^{*})
\end{bmatrix}
\end{equation*}
where,
\begin{equation*}
\boldsymbol{d}(\tau^{*}) =
\begin{bmatrix}
\frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} & 0 \\
0 & \frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} \\
\frac{-6r}{t^{2}} & 0 & \frac{4r}{t} & 0 \\
0 & \frac{-6r}{t^{2}} & 0 & \frac{4r}{t}
\end{bmatrix}\left \{
\boldsymbol{x}_f -
\begin{bmatrix}
1 & 0 & t & 0 \\
0 & 1 & 0 & t \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{x}_i \right \},
\end{equation*}
where $r$ represents the input weight, $\boldsymbol{x}$ represent the state, that is $[x, y, \dot{x}, \dot{y}]^{T}$, and $\tau^{*}$ represent the optimal arrival time.

The cost of the optimal control policy, $c^{*}$, needed in line $6$ and $12$, is calculated using the cost of optimal control for double integrator model in equation \ref{dbl_int_cost_eq}:

\begin{equation*}
c(\tau^{*}) =
\tau^{*} +
\left \{  
\boldsymbol{x}_f -
\begin{bmatrix}
1 & 0 & t & 0 \\
0 & 1 & 0 & t \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{x}_i
\right \}^{T}
\begin{bmatrix}
\frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} & 0 \\
0 & \frac{12r}{t^{3}} & 0 & \frac{-6r}{t^{2}} \\
\frac{-6r}{t^{2}} & 0 & \frac{4r}{t} & 0 \\
0 & \frac{-6r}{t^{2}} & 0 & \frac{4r}{t}
\end{bmatrix}
\left \{  
\boldsymbol{x}_f -
\begin{bmatrix}
1 & 0 & t & 0 \\
0 & 1 & 0 & t \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix}
\boldsymbol{x}_i
\right \}
\end{equation*}

The TrajectoryCollisionCheck procedure in line $5$ and line $6$ is the algorithm \ref{trajectory_collision_alg} which takes the trajectory of the optimal control policy, $\pi^{*}$. Line $6$ of the algorithm select the neighboring states of the sampled state that has lower cost among the other.

After the tree is grown, the solution trajectory, $\boldsymbol{q}_{solution}[]$ is returned by backtracking the goal state to initial state in line $17$ to $20$. In this work, we use KD-Tree representation for the tree $\mathcal{T}$ using \emph{nanoflann} library \cite{blanco2014nanoflann}.
		
\section{Motion Planning Strategy}
In this section, we explained our motion planning strategy for the omnidirectional mobile robot in dynamic Robot Soccer Environment. Figure \ref{block_diagram} illustrates the proposed strategy. The main idea of the strategy is to separate the trajectory planning for translational motion and rotational motion, taking advantages of the holonomic property of the omnidirectional mobile robot. This decoupling of translational motion and rotational motion planning also gives advantage to simplify the collision checking. Moreover, the robot is then model as a point with double integrator model while the obstacles are modeled as a circles with constant velocity. This simplification leads to simpler algorithm since only translational motion contribute to the collision while the rotational motion can be solved afterwards.

\begin{figure}[H]
	\centering
	\includegraphics[height=9cm]{block}
	\caption{Proposed Motion Planning Strategy.}
	\label{block_diagram}
\end{figure}

In figure \ref{block_diagram}, the planner takes the initial state of the robot, $q=[x,y,\omega,\dot{x},\dot{y},\dot{\omega}]^T$, and the desired goal position with zero velocity, $g=[x_{goal},y_{goal},\omega_{goal},0.0,0.0,0.0]^T]$. The initial translational state, $[x,y,\dot{x},\dot{y}]^T$, and translational goal position, $x_{goal},y_{goal}$ with zero velocity, is then fed to the kinodynamic-RRT* to produce translational trajectory plan. While the initial rotational state, $[\omega,\dot{\omega}]^T$, and the goal rotational state, $[\omega_{goal},0.0]^T$, is taken by the minimum-time trajectory generator to produce rotational trajectory plan. To simplify the collision checking procedure, the robot is modeled as point and the obstacles are modeled as circles, so that the rotational motion does not contribute for collision checking algorithm. Hence, information of dynamic obstacles is passed only from the environment to the kinodynamic-RRT*. The final trajectory plan then includes translational and rotational motion plan.

The trajectory planning for the translational motion is done by the kinodynamic-RRT* with double integrator model, while the trajectory planning for the rotational motion is done by minimum-time trajectory generator with velocity and acceleration constraints. The complete algorithm of the proposed motion planning strategy is shown in algorithm \ref{alg_motion_plan}.	

\begin{algorithm}[H]
\begin{algorithmic}[1]
\Function{MotionPlan}{$\boldsymbol{p}_{start}, \boldsymbol{v}_{start},\boldsymbol{p}_{goal}, n_{iter}$}
	\State $(x[],y[],\dot{x}[],\dot{y}[])\leftarrow \textsc{Kinodynamic-RRT*}(p_x,p_y,v_x,v_y,p_{x,goal},p_{y,goal})$ 
	\State $(\omega[],\dot{\omega}[]) \leftarrow \textsc{TrajectoryGenerator}(p_{\omega},v_{\omega},p_{\omega,goal})$
	\Return $(x[],y[],\omega[],\dot{x}[],\dot{y}[],\dot{\omega}[])$
\EndFunction
\end{algorithmic}
\caption{Proposed Motion Planning Strategy for Omnidirectional Mobile Robot}
\label{alg_motion_plan}
\end{algorithm}

The algorithm \ref{alg_motion_plan} takes the initial state $\boldsymbol{p}_{start}=[x,y,\omega]^T,\;\boldsymbol{v}_{start}=[\dot{x},\dot{y},\dot{\omega}]^T$ and the goal state $\boldsymbol{p}_{goal}=[x_{goal},y_{goal},\omega_{goal}]^T$. Then, the collision-free translational trajectory is planned using Kinodynamic-RRT* (algorithm \ref{rrt_alg}) with double integrator model while the rotational trajectory is generated using TrajectoryGenerator algorithm in \ref{trajectory_alg} with velocity and acceleration constraint.

\section{Trajectory Tracking for Omnidirectional Mobile Robot}
In this section, we discuss the implemented trajectory tracking for omnidirectional mobile robot. In this work, we use 'outer-loop' Trajectory Tracking as proposed in \cite{liu2008omni} to control the robot to follow the planned trajectory as reference. The relationship of the velocity of the robot in the body frame and world frame at time $t$ is expressed as :

\begin{equation}
\begin{bmatrix}
u_d(t)\\ 
v_d(t)\\ 
r_d(t)
\end{bmatrix}
=
\begin{bmatrix}cos(\omega_d(t)) & sin(\omega_d(t)) & 0 \\ 
-sin(\omega_d(t)) & cos(\omega_d(t)) & 0 \\ 
0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
\dot{x}_{d}(t) \\
\dot{y}_{d}(t) \\
\dot{\omega}_{d}(t) 
\end{bmatrix}
\end{equation}
where $[\dot{x}_d(t), \dot{y}_d(t), \dot{\omega}_d(t)]^T$ is the velocity of the desired trajectory in the world frame at time $t$.

Unlike the original trajectory tracking as proposed in \cite{liu2008omni}, we do not use pseudo-differentiator to approximate the derivative of the desired trajectory since the outputted trajectory from algorithm \ref{alg_motion_plan} satisfying the dynamics of the robot.

The error of trajectory is defined as :

\begin{equation*}
\begin{bmatrix}
e_x\\ 
e_y\\ 
e_\omega
\end{bmatrix} = 
\begin{bmatrix}
x(t)\\ 
y(t)\\ 
\omega(t)
\end{bmatrix} - 
\begin{bmatrix}
x_d(t)\\ 
y_d(t)\\ 
\omega_d(t)
\end{bmatrix}
\end{equation*}
where $[x(t), y(t), \omega(t)]^{T}$ is the position of the robot at time $t$, while $[x_d(t),y_d(t),\omega_d(t)]^{T}$ is the position of the desired trajectory at time $t$.
The error dynamics could be calculated using \cite{liu2008omni}:

\begin{equation*}
\begin{bmatrix}
\dot{e}_x\\ 
\dot{e}_y\\ 
\dot{e}_{\omega}
\end{bmatrix} = A_1(t)
\begin{bmatrix}
e_x\\ 
e_y\\ 
e_{\omega}
\end{bmatrix} + B_1(t)
\begin{bmatrix}
\tilde{u}\\ 
\tilde{v}\\ 
\tilde{r}
\end{bmatrix}
\end{equation*}
where

\begin{equation}
A_1(t)=
\begin{bmatrix}
0 & 0 & -u_d(t)sin(\omega_d(t)) - v_d(t)cos(\omega_d(t)) \\ 
0 & 0 & u_d(t)cos(\omega_d(t))-v_d(t)sin(\omega_d(t)) \\ 
0 & 0 & 0
\end{bmatrix}
\end{equation}

\begin{equation}
B_1(t) =
\begin{bmatrix}cos(\omega_d(t)) & -sin(\omega_d(t)) & 0 \\ 
sin(\omega_d(t)) & cos(\omega_d(t)) & 0 \\ 
0 & 0 & 1
\end{bmatrix}
\end{equation}

\begin{equation*}
\begin{bmatrix}
\tilde{u} \\ 
\tilde{v} \\
\tilde{r}
\end{bmatrix} =
\begin{bmatrix}
u \\ v \\ r
\end{bmatrix} -
\begin{bmatrix}
u_d(t) \\ v_d(t) \\ r_d(t)
\end{bmatrix}
\end{equation*}

In \cite{liu2008omni}, to stabilize the tracking error, a proportional-integral (PI) feedback control is proposed:

\begin{equation}
\begin{bmatrix}
\tilde{u} \\ \tilde{v} \\ \tilde{r}
\end{bmatrix} = 
-K_{P}
\begin{bmatrix}
e_x \\ e_y \\ e_{\omega}
\end{bmatrix}
-K_{I}
\begin{bmatrix}
\int e_x(t)dt \\
\int e_y(t)dt \\
\int e_{\omega}(t)dt
\end{bmatrix}
\end{equation}
where $K_P$ is a constant for proportional term and $K_I$ is a constant for integral term.
And then the augmented tracking error vector is defined by :

\begin{equation*}
\gamma = 
\begin{bmatrix}
\gamma_1 \\
\gamma_2 \\
\gamma_3 \\
\gamma_4 \\
\gamma_5 \\
\gamma_6
\end{bmatrix} = 
\begin{bmatrix}
\int e_x(t)dt \\
\int e_y(t)dt \\
\int e_{\omega}(t)dt \\
e_x \\
e_y \\
e_{\omega} \\
\end{bmatrix}
\end{equation*}
so that the closed-loop tracking error state equation becomes :
\begin{equation*}
\dot{\gamma} = A_{1c}\gamma
\end{equation*}
where
\begin{equation*}
A_{1c}=
\begin{bmatrix}
O & I \\
-B_1 K_I & A_1-B_1 K_P
\end{bmatrix}
\end{equation*}

From \cite{liu2008omni}, the proportional and integral constant is selected to achieve the following closed-loop tracking error dynamics :

\begin{equation*}
A_{1c}=
\begin{bmatrix}
O & I \\
\begin{bmatrix}
-a_{I1} & 0 & 0 \\
0 & -a_{I2} & 0 \\
0 & 0 & -a_{I3}
\end{bmatrix} &
\begin{bmatrix}
-a_{P1} & 0 & 0 \\
0 & -a_{P2} & 0 \\
0 & 0 & -a_{P3}
\end{bmatrix}
\end{bmatrix}
\end{equation*}
so that $K_{I}$ and $K_{P}$ could be expressed as :

\begin{equation}
K_{I} = 
-B_{1}^{-1} 
\begin{bmatrix}
-a_{I1} & 0 & 0 \\
0 & -a_{I2} & 0 \\
0 & 0 & -a_{I3}
\end{bmatrix}
\end{equation}

\begin{equation}
K_{P} = 
-B_{1}^{-1} 
( A_1 - 
\begin{bmatrix}
-a_{P1} & 0 & 0 \\
0 & -a_{P2} & 0 \\
0 & 0 & -a_{P3}
\end{bmatrix}
)
\end{equation}

Finally, the commanded velocity in the body frame is given by :

\begin{equation}
\begin{bmatrix}
u_{com} \\
v_{com} \\
r_{com} 
\end{bmatrix}
=
\begin{bmatrix}
u_d \\
v_d \\
r_d
\end{bmatrix}
+
\begin{bmatrix}
\tilde{u} \\
\tilde{v} \\
\tilde{r}
\end{bmatrix}
\end{equation}

\section{Online Computation of Motion Planning and Trajectory Tracking}

In this section, we describe our approach to deal with changing environment while solving the collision-free solution trajectory. Since the motion of the obstacles at any given point of time are not available, we iteratively solve the motion plan with the updated environment and concurrently control the robot to follows the updated trajectory. Figure \ref{online_loop} illustrates our online computation approach. With this approach, we could compute motion plan and track the trajectory separately at different rate.

\begin{figure}
	\centering
	\includegraphics[width=10cm]{online_loop}
	\caption{Online Computation of Motion Planning and Trajectory Tracking}
	\label{online_loop}
\end{figure}

In figure \ref{online_loop}, each ellipses illustrate different processes that are executed concurrently and each box represents data that are in the form of ROS messages, with \vars{f sim}, \vars{f plan}, and \vars{f control} represents the loop rate of simulator, motion planning, and trajectory tracking loop, respectively. The simulator process, in this experiment, is the Gazebo simulator from standard ROS distribution. Motion planning loop illustrate our motion planning system's main computation loop that takes obstacles and robot state information from the simulator. Trajectory tracking loop illustrate our implementation the trajectory tracking control for omnidirectional robot. This loop takes the reference trajectory from the motion planning loop and the robot state from the simulator and publishing robot velocity control back to the simulator.

\begin{algorithm}[H]
\begin{algorithmic}[1]
\Function{OnlineMotionPlan}{}
	\State $\vars{subscriber} \leftarrow \func{create ROS subscriber}$
	\State $\vars{ok} \leftarrow \vars{True}$
	\While {$\vars{ok}$}
		\State $\boldsymbol{p}_{start}, \boldsymbol{v}_{start} \leftarrow \func{read}\; \vars{robot\_state}\; \func{from}\;\vars{subscriber}$
		\State $\boldsymbol{p}_{goal} \leftarrow \func{read}\; \vars{goal}\; \func{from}\;\vars{subscriber}$ 
		\State $\func{update}\;\vars{obstacles}\; \func{from}\;\vars{subscriber}$
		\State $\vars{trajectory} \leftarrow \textsc{MotionPlan}(\boldsymbol{p}_{start},\boldsymbol{v}_{start},\boldsymbol{p}_{goal})$ 
		\State $\func{publish}\; \vars{trajectory}$ 
		\State $\func{publish}\; \vars{tree}\; \func{visualization}$ 
		\State $\vars{ok} \leftarrow \vars{rosmaster}\; \func{is alive}$
	\EndWhile
\EndFunction
\end{algorithmic}
\caption{Online Motion Planning Loop}
\label{alg_online_plan}
\end{algorithm}

\begin{algorithm}[H]
\begin{algorithmic}[1]
\Function{OnlineTrajectoryTracking}{}
	\State $\vars{subscriber} \leftarrow \func{create ROS subscriber}$
	\State $\vars{ok} \leftarrow \vars{True}$
	\While {$\vars{ok}$}
		\State $t \leftarrow \func{current time}$
		\State $\vars{trajectory} \leftarrow \func{read}\; \vars{trajectory} \;\func{from}\; \vars{subscriber}$
		\State $[x, y, \omega]^T \leftarrow \func{get} \;\vars{robot\_state}\; \func{from}\; \vars{subscriber}$
		\State $[x_{ref}, y_{ref}, \omega_{ref}]^T,\; [\dot{x}_{ref}(t), \dot{y}_{ref}, \dot{\omega}_{ref}]^T \leftarrow \func{get} \;\vars{trajectory}\; \func{at}\;t$
		\State $[v_x,v_y,v_{\omega}]^T \leftarrow \textsc{PI-Control}([x, y, \omega]^T,[x_{ref}, y_{ref}, \omega_{ref}]^T,\; [\dot{x}_{ref}, \dot{y}_{ref}, \dot{\omega}_{ref}]^T)$
		\State $\func{publish} \; [v_x,v_y,v_{\omega}]^T$
		\State $\vars{ok} \leftarrow \vars{rosmaster}\; \func{is alive}$
	\EndWhile
\EndFunction
\end{algorithmic}
\caption{Online Trajectory Tracking Loop}
\label{alg_online_tracking}
\end{algorithm}

Algorithm \ref{alg_online_plan} and \ref{alg_online_tracking} shows the general procedure of the proposed online motion planning and trajectory tracking loop, respectively. In the algorithms, \vars{subscriber} is a ROS subscriber that listening to corresponding ROS topic. In the motion planning loop, \vars{robot\_state} are \vars{goal} retrieved from corresponding ROS topic and then used as initial and goal state of the motion planning algorithm, \textsc{MotionPlan}, as explained in the previous chapter. The environment is also updated by reading \vars{obstacles}' states from \vars{subscriber} and then is fed to collision checking algorithm before it is used by \textsc{MotionPlan}. Finally, the solution \vars{trajectory} and the grown \vars{tree} is published. The trajectory tracking loop first read the current time, that is then to be used for retrieving reference trajectory, and then read the planned trajectory and the current \vars{robot\_state} from \vars{subscriber}. Then, reference trajectory for current time is retrieved from the planned trajectory and is then used for computing the control velocity. In \ref{alg_online_tracking}, the robot state, $[x,y,\omega]^T$, and reference trajectory, $[x_{ref},y_{ref},\omega_{ref}]^T,[\dot{x}_{ref},\dot{y}_{ref},\dot{\omega}_{ref}]^T$, are in fixed frame while the control velocity, $[v_x,v_y,v_{\omega}]^T$, is in body frame. Finally, the control velocity is published.

\end{document}
